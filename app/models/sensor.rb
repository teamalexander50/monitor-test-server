class Sensor < ActiveRecord::Base

  belongs_to :operating_room
  has_many :open_events

end
