class OpenEvent < ActiveRecord::Base

  belongs_to :operating_room
  belongs_to :sensor

end
