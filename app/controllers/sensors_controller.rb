class SensorsController < ApplicationController

  def index
    unless params[:or_id].nil? or params[:sensor_id].nil?
      @sensor = Sensor.where(:operating_room_id => params[:or_id]).where(:id => params[:sensor_id]).first
    end

    respond_to do |format|
      format.all {render :json => @sensor.as_json}
    end

  end

  def register_open
    @event = OpenEvent.create(
        :operating_room_id => params[:or_id],
        :sensor_id => params[:sensor_id],
        :open_time => Time.zone.parse(params[:open_time]),
        :close_time => params[:seconds].to_f,
        :seconds => Time.zone.parse(params[:open_time]) + seconds
    )
    respond_to do |format|
      format.all { render :json => @event}
    end
  end

  def register_heartbeat
    unless params[:or_id].nil? or params[:sensor_id].nil? or params[:online].nil?
      @sensor = Sensor.where(:operating_room_id => params[:or_id], :id => params[:sensor_id]).first
      unless @sensor.nil?
        @sensor.update(:online => params[:online])
      end
    end

    respond_to do |format|
      format.all { render :json => {:heartbeat => params[:online]}}
    end
  end

end
