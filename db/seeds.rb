# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

or_7 = OperatingRoom.create(:name => 'Operating Room 7')
Sensor.create(:operating_room => or_7, :label => 'Main Door', :online => false)
Sensor.create(:operating_room => or_7, :label => 'Side Door 2', :online => false)
Sensor.create(:operating_room => or_7, :label => 'Side Door 1', :online => false)
Sensor.create(:operating_room => or_7, :label => 'Sterile Room Door', :online => false)