class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|

      t.references :operating_room
      t.string :label
      t.boolean :online

    end
  end
end
