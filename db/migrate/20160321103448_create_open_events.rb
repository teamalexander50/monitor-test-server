class CreateOpenEvents < ActiveRecord::Migration
  def change
    create_table :open_events do |t|

      t.references :operating_room
      t.references :sensor
      t.timestamp :open_time
      t.timestamp :close_time
      t.integer :seconds

    end
  end
end
